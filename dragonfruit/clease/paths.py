"""Module to store the paths to various directories in dragonfruit projects"""

import pyos

CLEASE_DIR = pyos.PurePath("_clease/")
STRUCTURE_SETTINGS = pyos.PurePath("structure_settings")
GENERAL_SETTINGS = pyos.PurePath("general_settings")
GROUP_SETTINGS = pyos.PurePath("group_settings")
