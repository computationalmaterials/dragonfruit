# ADF related constants

META_PRESUMED_CHARGE = "presumed_charge"  # Key used in dictionary to set charge of molecule
META_PARENT_MOLECULES = "parent_molecules"

# Keys for the adfprep settings
ADFPREP_CHARGE = "z"  # The system charge

# Constants for indicating where overall system charge should be taken from
ATOMS_META = "atoms_meta"  # Take charge from the META_PRESUMED_CHARGE key of the atoms' metadata
