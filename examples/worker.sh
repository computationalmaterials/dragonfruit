#!/usr/bin/env bash
#SBATCH -N 1                            # Number of nodes
#SBATCH --ntasks-per-node=40            # Number of cores per node
#SBATCH -p xeon40                       # Pertition name
#SBATCH -t 2-02:00:00                   # Time (50h is max on most partitions)
#SBATCH -J job-name
#SBATCH --mail-type=END,FAIL            # Send an e-mail on completion of any kind
#SBATCH --mail-user=<your-email>        # E-mail for SLURM logs

################################################
# This is a template for a submission script
# Adjust as needed
################################################

#https://stackoverflow.com/questions/242538/unix-shell-script-find-out-which-directory-the-script-file-resides
# Get absolute script location
SCRIPT=$(readlink -f "$0")
sbatch $SCRIPT  # Resubmit this script

module load VASP
# Run over several queues, for as long as there is anything in the queue
for QUEUE in 'xeon40-high' 'xeon40' 'default-queue'; do
    fruit run -n -1 $QUEUE
done
