# Dragonfruit
[![pipeline status](https://gitlab.com/computationalmaterials/dragonfruit/badges/main/pipeline.svg)](https://gitlab.com/computationalmaterials/dragonfruit/-/commits/main)
[![Documentation Status](https://readthedocs.org/projects/dragonfruit/badge/?version=latest)](https://dragonfruit.readthedocs.io/en/latest/?badge=latest)

A workflow tool for simulation software. For more information, please see our [documentation](https://dragonfruit.readthedocs.io/en/latest/).
