from ase.build import molecule
from ase.calculators.vasp import Vasp

atoms = molecule("H2", vacuum=5)

calc = Vasp(
    xc="LDA",
    ibrion=1,
    nsw=50,
    ediffg=-0.1,
    ediff=1e-3,
)
atoms.calc = calc
atoms.get_potential_energy()
