from dragonfruit import pyos_utils as utils


def test_sanitize():
    meta = {"foo": "bar"}
    assert utils.sanitize_meta(meta) == meta

    meta = {"foo": "bar", "_directory": "/somedir/", "name": "fiz"}
    sanitized = utils.sanitize_meta(meta)
    assert len(sanitized) == 1
    assert "_directory" not in sanitized
    assert "name" not in sanitized
