import pytest
from ase.build import bulk
from dragonfruit.utils import get_atoms_sort_map


@pytest.fixture
def atoms(historian):
    # Order: Na, Cl, Na, Cl, ...
    return bulk("NaCl", crystalstructure="rocksalt", a=5.0, cubic=True) * (2, 2, 2)


def test_sorting_atoms(atoms):
    # Make sure atoms are in alternating order first as it should be
    symbols = ("Na", "Cl")
    for i, symbol in enumerate(atoms.symbols):
        assert symbols[i % 2] == symbol

    srt, resrt = get_atoms_sort_map(atoms)
    # Ensure that we are now ordered
    assert all(atoms[srt][:32].symbols == "Na")
    assert all(atoms[srt][32:].symbols == "Cl")
    assert atoms == atoms[srt][resrt]
