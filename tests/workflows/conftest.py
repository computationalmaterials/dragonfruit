import os
import pytest

from ase.calculators.singlepoint import SinglePointCalculator
from ase.calculators.vasp import Vasp
from dragonfruit import vasp
from dragonfruit.vasp import get_default_handlers

DRAGONFRUIT_VASP_COMMAND = "DRAGONFRUIT_VASP_COMMAND"
DRAGONFRUIT_PP_PATH = "VASP_PP_PATH"


@pytest.fixture
def vasp_command():
    cmd = os.environ.get(DRAGONFRUIT_VASP_COMMAND, None)
    if cmd is None:
        pytest.skip(
            "No VASP command. Set environment variable {} to enable".format(
                DRAGONFRUIT_VASP_COMMAND
            )
        )
    return cmd


# Cannot use marks on fixtures, so we instead just make this a fixture
@pytest.fixture
def must_have_pp_path():
    if not bool(os.environ.get(DRAGONFRUIT_PP_PATH, False)):
        pytest.skip(f"Must have env var {DRAGONFRUIT_PP_PATH} set for Pseudopotentials.")


@pytest.fixture
def vaspcalc(vasp_command, must_have_pp_path):
    def _vaspcalc(**kwargs):
        calc = Vasp(command=vasp_command, **kwargs)
        return calc

    return _vaspcalc


@pytest.fixture
def unset_vasp_envvars(monkeypatch):
    """Ensure that the envvars used to launch a VASP calculation aren't set"""
    for key in (
        "VASP_COMMAND",
        "ASE_VASP_COMMAND",
        "VASP_PP_PATH",
        DRAGONFRUIT_VASP_COMMAND,
    ):
        monkeypatch.delenv(key, raising=False)


@pytest.fixture
def mock_vasp_initialize(mocker):
    def _mock_initialize(self, atoms):
        self.spinpol = False
        self.sort = range(len(atoms))
        self.resort = range(len(atoms))
        self.atoms = atoms

    mocker.patch(
        "ase.calculators.vasp.create_input.GenerateVaspInput.initialize",
        _mock_initialize,
    )
    yield


@pytest.fixture
def mock_calculate(mocker, unset_vasp_envvars, mock_vasp_initialize):
    """Fixture where the calculate method is mocked, and just does a read_results()"""

    def _mock_calculate(self, atoms=None, properties=("energy",), system_changes=("energy",)):
        if atoms is None:
            atoms = self.atoms
        self.initialize(atoms)
        self.read_results()

    mocker.patch("ase.calculators.vasp.Vasp.calculate", _mock_calculate)
    yield


@pytest.fixture
def vasptask_factory():
    def _make_vasptask(atoms, settings=None, with_handlers=False, **kwargs):
        default_settings = {"xc": "LDA"}
        if settings:
            default_settings.update(**settings)
        task = vasp.VaspTask(atoms, default_settings, **kwargs)
        if with_handlers:
            restarter = task.get_restarter()
            restarter.register_many(get_default_handlers())
        return task

    return _make_vasptask


@pytest.fixture
def vasprun_factory():
    def _make_vasprun(atoms, results=None, **kwargs):
        run = vasp.VaspRun(atoms, {"xc": "LDA"}, **kwargs)
        if results:
            # Spoof vasp run as converged and done
            run.post_calculation_state = {vasp.CRASHED: False, vasp.CONVERGED: True}
            final_atoms = atoms.copy()
            calc = SinglePointCalculator(final_atoms, **results)
            final_atoms.calc = calc
            run._atom_steps.append(atoms)
        return run

    return _make_vasprun
