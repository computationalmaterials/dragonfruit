import copy
import io
import os
import shutil
import pytest

import ase
import ase.io
from ase.calculators.vasp import Vasp
import mincepy
import mincepy_sci

import dragonfruit as df
from dragonfruit.vasp import VaspTask
import dragonfruit.vasp.vasp_parsers as vp
import dragonfruit.mince

# pylint: disable=invalid-name


@pytest.fixture
def vasp_run(atoms, tmp_path):
    return df.vasp.VaspRun(atoms, {"xc": "LDA"}, tmp_path)


@pytest.fixture
def scheduler_env():
    """Get a made up scheduler environment"""
    return df.SchedulerEnvironment("slurm", "my_job.out", "my_job.err", {"bang": "tidy"})


def test_vasp_run(atoms, scheduler_env, tmp_path, historian: mincepy.Historian):
    d = 2.9
    L = 10.0
    vasp_run = df.vasp.VaspRun(atoms, {"xc": "LDA"}, tmp_path)
    # Manually set the environment
    vasp_run._scheduler_env = scheduler_env
    vasp_id = historian.save(vasp_run)
    del vasp_run

    loaded_vasp = historian.load(vasp_id)
    assert all(loaded_vasp.atoms.positions[0] == atoms.positions[0])
    assert all(loaded_vasp.atoms.cell.lengths() == [d, L, L])
    assert loaded_vasp.scheduler_env == scheduler_env


def test_vasp_task(vasp_run, atoms, historian: mincepy.Historian):
    task = df.vasp.VaspTask(atoms, {"xc": "LDA"})
    task_id = historian.save(task)

    task.runs.append(vasp_run)
    historian.save(task)

    loaded_task = historian.load(task_id)
    assert loaded_task is task


def test_scheduler_environment(scheduler_env, historian: mincepy.Historian):
    env_copy = copy.copy(scheduler_env)

    env_id = historian.save(scheduler_env)
    del scheduler_env
    loaded = historian.load(env_id)
    # Compare directly rather than the __eq__ method as we want to test if that works too!
    assert loaded.scheduler == env_copy.scheduler
    assert loaded.stdout == env_copy.stdout
    assert loaded.stderr == env_copy.stderr
    assert loaded.extra == env_copy.extra
    # Now check the __eq__ method
    assert loaded == env_copy


def test_vasp_run_update(test_files_path, historian: mincepy.Historian):
    vasp_path = test_files_path / "H2"
    initial_atoms = ase.io.read(vasp_path / "POSCAR")

    run = df.vasp.VaspRun.from_vasp_run(vasp_path)
    assert run.initial_atoms == initial_atoms
    assert len(run.atoms_history) == 4

    assert run.atoms is run.atoms_history[3]
    assert run.results is run.results_history[3]

    # Now make sure we can save and load this thing
    run_id = historian.save(run)
    del run
    loaded = historian.load(run_id)
    assert loaded.initial_atoms == initial_atoms
    assert len(loaded.atoms_history) == 4

    assert loaded.atoms is loaded.atoms_history[3]
    assert loaded.results is loaded.results_history[3]


def test_vasp_run(test_files_path, unset_vasp_envvars):
    """Do a run without proper setup, we should get to the point where
    ASE tries to run"""
    vasp_path = test_files_path / "H2"
    calc = Vasp(restart=True, directory=vasp_path)
    settings = calc.todict()
    atoms = calc.get_atoms()

    vasp_task = df.vasp.VaspTask(atoms, settings)
    vasp_run = vasp_task.new_run(run_path=vasp_path)
    with pytest.raises(ase.calculators.calculator.CalculatorSetupError):
        vasp_run.run()


def test_vasp_fake_run(test_files_path, historian: mincepy.Historian):
    vasp_path = test_files_path / "H2"
    calc = Vasp(restart=True, directory=vasp_path)
    settings = calc.todict()
    atoms = calc.get_atoms()

    vasp_task = df.vasp.VaspTask(atoms, settings)
    vasp_run = vasp_task.new_run(run_path=vasp_path)
    os.environ["ASE_VASP_COMMAND"] = "echo 0"
    vasp_run.fake_run()
    historian.save(vasp_task)

    assert len(vasp_run.atoms_history) == 4
    assert len(vasp_run.results_history) == 4
    assert vasp_run.converged

    # Now pretend we're going to do a follow up run
    subsequent_run = vasp_task.new_run()
    assert subsequent_run.atoms == vasp_run.atoms


def test_vasp_is_converged_reader(test_files_path):
    vasp_path = test_files_path / "OUTCAR_tests"
    # Pairs of filename and the expected answer
    filenames = {
        "OUTCAR_relax_ok.gz": True,
        "OUTCAR_nsw_reached.gz": False,
        "OUTCAR_nelm_reached.gz": False,
        "OUTCAR_singlepoint.gz": True,
    }
    for filename, answer in filenames.items():
        converged = vp.read_is_converged(vasp_path / filename)
        assert converged == answer


def test_atoms_saving_calculator(test_files_path, historian: mincepy.Historian):
    vasp_path = test_files_path / "H2"
    calc = Vasp(restart=True, directory=vasp_path)
    atoms = calc.get_atoms()

    atoms_id = historian.save(atoms)
    del atoms

    loaded = historian.load(atoms_id)
    # This time around it should be single point
    assert isinstance(loaded.calc, ase.calculators.singlepoint.SinglePointCalculator)

    del loaded
    # This time ask it to load the original calculator
    historian.register_type(mincepy_sci.ase_types.AtomsHelper(load_original_calculator=True))
    reloaded = historian.load(atoms_id)
    assert isinstance(reloaded.calc, ase.calculators.vasp.Vasp)


def test_vasp_task_new_run(atoms, tmp_path, historian: mincepy.Historian):
    task = VaspTask(atoms, {"xc": "LDA"})
    assert len(task.runs) == 0
    task_id = historian.save(task)

    run = task.new_run(description="Yo momma", run_path=tmp_path)
    assert len(task.runs) == 1
    assert task.runs[0] is run
    historian.save(task)
    del task

    loaded = historian.load(task_id)
    assert len(loaded.runs) == 1
    assert loaded.runs[0] is run

    loaded.new_run()
    loaded_id = historian.save(loaded)
    assert loaded_id == task_id
    del loaded
    historian.load(loaded_id)


def test_vasp_run_mutation_saving(atoms, tmp_path, historian: mincepy.Historian):
    task = VaspTask(atoms, {"xc": "LDA"})
    assert len(task.runs) == 0
    run = task.new_run(description="Test run", run_path=tmp_path)
    assert len(task.runs) == 1
    task_id = historian.save(task)

    # Now mutate the run and make sure that the task is saved again
    run.set_result(None)
    assert run.done
    historian.save(task)
    del task, run

    loaded = historian.load(task_id)
    assert loaded.get_last_run().done


def test_vasp_run_files(test_files_path, historian: mincepy.Historian):
    vasp_path = test_files_path / "H2"
    run = df.vasp.VaspRun.from_vasp_run(vasp_path)

    # Now make sure we can save and load this thing, keeping the files
    initial_files = set(run.keep_files)
    assert initial_files
    assert not run.stored_files, "Should not be any stored files before save"
    run_id = historian.save(run)
    del run

    # Load and compare
    loaded = historian.load(run_id)
    loaded_files = set(loaded.stored_files.keys())
    assert loaded_files == initial_files

    initial_incar = io.StringIO()
    loaded_incar = io.StringIO()

    with open(vasp_path / "INCAR", "r") as file:
        shutil.copyfileobj(file, initial_incar)

    with loaded.stored_files["INCAR"].open("r") as file:
        shutil.copyfileobj(file, loaded_incar)

    assert initial_incar.getvalue() == loaded_incar.getvalue()


def test_vasp_run_files_save_twice(test_files_path, historian: mincepy.Historian):
    vasp_path = test_files_path / "H2"
    run = df.vasp.VaspRun.from_vasp_run(vasp_path)

    # Now make sure we can save and load this thing, keeping the files
    initial_files = set(run.keep_files)
    assert initial_files
    assert not run.stored_files, "Should not be any stored files before save"
    historian.save(run)
    historian.save(run)


def test_vasp_run_overwrite(tmp_path, test_files_path):
    with df.utils.working_directory(tmp_path):
        vasp_path = test_files_path / "H2"
        run = df.VaspRun.from_vasp_run(vasp_path)
        assert run.settings["gamma"] is False

        run.settings_overwrites["gamma"] = True
        assert run.run_settings["gamma"] is True
        assert run.settings["gamma"] is False

        # A restart should have no overwrites but the settings of the original
        restart = run.create_restart()
        assert restart.settings["gamma"] is False
        assert restart.run_settings["gamma"] is False
        assert not restart.settings_overwrites


def test_vasp_error_registration(atoms):
    task = df.vasp.VaspTask(atoms, {"xc": "LDA"})
    restarter = task.get_restarter()
    handlers = df.vasp.get_default_handlers()
    restarter.register_many(handlers)

    handlers = sorted(handlers, reverse=True, key=lambda x: x[1])

    assert len(handlers) == len(task.get_restarter()._all_check_functions)
    for expected_handler_prio, entry in zip(handlers, task.get_restarter()._all_check_functions):
        prio, handler = entry
        expected_handler, expected_prio = expected_handler_prio
        assert isinstance(handler, df.vasp.ErrorHandler)
        assert expected_prio == prio
        # Check that the handler has been copied
        assert handler is not expected_handler
        # Ensure that the handler is still the expected type
        assert isinstance(handler, type(expected_handler))


def test_vasp_task_vacancy(atoms, tmp_path, historian: mincepy.Historian):
    def assert_vac_count(atoms, expected):
        """Count number of vacancies, and assert we found
        the expected number"""
        c = sum(1 for atom in atoms if atom.symbol == "X")
        assert c == expected

    assert_vac_count(atoms, 0)
    atoms += ase.Atom("X")
    assert_vac_count(atoms, 1)

    task = VaspTask(atoms, {"xc": "LDA"})
    assert_vac_count(task.initial_atoms, 1)
    historian.save(task)  # Test we can save OK
    run = task.new_run(description="Test run", run_path=tmp_path)

    assert_vac_count(run.initial_atoms, 0)
    os.environ["ASE_VASP_COMMAND"] = "echo 0"
    run.fake_run()
    assert_vac_count(run.atoms, 0)
    assert_vac_count(atoms, 1)  # Test we didn't mutate the initial
