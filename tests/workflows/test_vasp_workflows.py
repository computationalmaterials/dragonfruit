import pytest
import mincepy
import pyos
from pyos import psh

import dragonfruit as df
from dragonfruit.vasp import workflows as wf


@pytest.fixture
def dummy_workflow(mocker):
    """Fixture for mocking the run function of the SingleTaskWorkflow and
    VolumeMetaConvergence class"""

    def _run(self):
        self.set_result(True)

    mocker.patch("dragonfruit.vasp.workflows.VolumeMetaConvergence.run", _run)
    mocker.patch("dragonfruit.vasp.workflows.SingleTaskWorkflow.run", _run)


def test_simple_vasp_task(atoms, historian: mincepy.Historian):
    task = df.vasp.SimpleVaspWorkflow(atoms, {"xc": "LDA"}, historian)
    task_id = historian.save(task)
    del task

    historian.load(task_id)


def test_volume_meta_convergence(atoms, historian: mincepy.Historian):
    task = wf.VolumeMetaConvergence(atoms, {"xc": "LDA"})
    task_id = historian.save(task)
    del task

    loaded = historian.load(task_id)
    assert isinstance(loaded, wf.VolumeMetaConvergence)


def test_volume_converger_task_insertion(atoms, vasprun_factory, historian):
    converger = wf.VolumeMetaConvergence(atoms, {"xc": "LDA"})
    conv_id = historian.save(converger)

    assert converger.atoms is atoms

    converger._create_new_task()

    # Test that the new task is correctly saved as well
    del converger
    converger = historian.load(conv_id)

    assert len(converger.task_list) == 1
    assert converger.converged is False

    # Let's populate the task, spoof it as complete
    task = converger.task_list[0]
    task.append_run(vasprun_factory(atoms, results={"energy": -2}))
    assert converger.converged is False

    # Insert another task - minimum requirement for convergence
    converger._create_new_task()
    assert len(converger.task_list) == 2
    assert converger.task_list[0] is not converger.task_list[1]
    assert converger.task_list[0].atoms == converger.task_list[1].atoms

    task = converger.task_list[1]
    task.append_run(vasprun_factory(atoms, results={"energy": -2}))

    assert converger.converged

    assert task is converger.get_vasp_task()


def test_converger_chain(atoms, dummy_workflow, historian):
    settings_changes = ({"xc": "lda"}, {"xc": "pbe"})
    base_settings = {}

    converger = wf.ConvergenceWorkflowChain(atoms, base_settings, settings_changes)
    conv_id = historian.save(converger)

    # Test that we created the first workflow, and only the first workflow
    # Nothing should've been done to it yet
    assert converger.num_workflows == 1
    assert isinstance(converger.workflows[0], wf.VolumeMetaConvergence)
    assert converger.workflows[0].initial_settings == settings_changes[0]
    assert converger.initial_atoms == atoms
    assert converger.workflows[0].initial_atoms == atoms
    assert not converger.done
    assert not converger.workflows[0].done

    converger.run()
    assert converger.num_workflows == 2
    assert converger.done
    for flow in converger.workflows:
        assert flow.done
    assert converger.workflows[1].initial_settings == settings_changes[1]

    del converger

    converger = historian.load(conv_id)

    assert converger.num_workflows == 2
    assert converger.done
    assert converger.workflows[0].initial_settings == settings_changes[0]
    assert converger.workflows[1].initial_settings == settings_changes[1]


def test_single_task_workflow(atoms, dummy_workflow, historian):
    converger = wf.SingleTaskWorkflow(atoms, {"xc": "LDA"})
    assert converger.converged is False
    converger.run()

    conv_id = historian.save(converger)
    del converger
    converger = historian.load(conv_id)
    assert isinstance(converger, wf.SingleTaskWorkflow)
    assert converger.done is True
    assert converger.converged is False


def test_single_task_workflow_convergence(atoms, vasprun_factory, pyos_project, historian):
    converger = wf.SingleTaskWorkflow(atoms, {"xc": "LDA"})
    task = converger.vasp_task
    task.append_run(vasprun_factory(atoms, results={"energy": -2}))

    assert converger.converged

    conv_id = historian.save(converger)
    del converger
    converger = historian.load(conv_id)
    assert isinstance(converger, wf.SingleTaskWorkflow)
    assert converger.converged is True

    # Try loading the task, since it's a reference
    task_id = psh.oid(task)
    del task
    task = historian.load(task_id)
    assert isinstance(task, df.vasp.VaspTask)
    assert task.converged
    assert task is converger.get_vasp_task()

    # Task and workflow should be located in the same folder
    pa_conv = pyos.Path(pyos.db.get_path(conv_id))
    pa_task = pyos.Path(pyos.db.get_path(task_id))
    assert pa_conv.parent == pa_task.parent
