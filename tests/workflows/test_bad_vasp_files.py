"""Test that we can deal with some examples of bad VASP files, without exploding"""

import pytest
from pytest_cases import fixture_ref, parametrize
import ase
from ase.io import read

from ase.utils import workdir
import dragonfruit.vasp.vasp_default_errors as ve  # ve: vasp-errors
from dragonfruit.vasp.vasp_errors import PermanentFailure
import dragonfruit.vasp.vasp_parsers as vp


@pytest.fixture
def corrupt_test_files(test_files_path):
    return test_files_path / "corrupt_test_files"


@pytest.fixture
def pricel_error_files(corrupt_test_files):
    return corrupt_test_files / "pricel_error"


@pytest.fixture
def posmap_error_files(corrupt_test_files):
    return corrupt_test_files / "posmap_error"


@pytest.fixture
def zbrent_error_files(corrupt_test_files):
    return corrupt_test_files / "zbrent_error"


@parametrize(
    "path, restarter_cls",
    [
        (fixture_ref("pricel_error_files"), ve.Pricel),
        (fixture_ref("posmap_error_files"), ve.Posmap),
        (fixture_ref("zbrent_error_files"), ve.Zbrent),
    ],
)
def test_corrupt_files_simple_adjustment(path, restarter_cls, vasptask_factory, mock_calculate):
    """Helper fixture for applying test to SimpleAdjustment type error handlers"""
    with workdir(path):
        initial_atoms = read("POSCAR")
        # Set up a VASP task, and register the standard error handlers
        vasp_task = vasptask_factory(initial_atoms, with_handlers=True)

        # Create a run, and simulate the execution with the mocked calculator
        run = vasp_task.new_run()
        run.run()

        # Apply restarter
        restarter = vasp_task.get_restarter()
        run = restarter.create_restart(vasp_task)

        # Verify that the restarter class was used
        assert restarter_cls.NAME in run.description
        for key, value in restarter_cls.ADJUSTMENT.items():
            assert run.settings[key] == value


def test_zbrent_alternative(zbrent_error_files, vasptask_factory, mock_calculate):
    zbrent = ve.ZbrentAlternative(max_count=1)
    with workdir(zbrent_error_files):
        initial_atoms = read("POSCAR")
        # Set up a VASP task, and register the standard error handlers
        vasp_task = vasptask_factory(initial_atoms, with_handlers=False)
        restarter = vasp_task.get_restarter()

        restarter.register(zbrent)

        prev_run = vasp_task.new_run()
        prev_run.run()

        # Apply restarter
        run = restarter.create_restart(vasp_task)

        assert zbrent.NAME in run.description
        # We should have the penultimate atoms object, not the lat
        assert run.initial_atoms == prev_run.atoms_history[-2]
        assert run.initial_atoms != prev_run.atoms

        # We set max_count to 1, we shouldn't be able to restart again.
        with pytest.raises(PermanentFailure):
            restarter.create_restart(vasp_task)


@parametrize("path", [fixture_ref("pricel_error_files"), fixture_ref("posmap_error_files")])
def test_is_corrupt(path):
    assert vp.is_xml_corrupt(path / "vasprun.xml")
    assert vp.is_outcar_corrupt(path / "OUTCAR")
    assert vp.is_output_corrupt(path)


def test_partial_corrupt_outcar(corrupt_test_files):
    outcar = corrupt_test_files / "OUTCAR_string_to_float_error.gz"

    # We can read the last image, so should be OK
    assert not vp.is_outcar_corrupt(outcar)
    # Check ASE can read the last image
    assert isinstance(read(outcar, index=-1), ase.Atoms)

    # ASE can now read the entire thing
    # without crashing, although it should raise a warning
    with pytest.warns(UserWarning):
        images = read(outcar, index=":")

    # We read try and read as many chunks as we can.
    # Verify that we should have 95 images in total
    # use vp.open_file to open compressed file
    count = 0
    with vp.open_file(outcar) as file:
        for line in file:
            if line.strip().startswith("FREE ENERGIE"):
                count += 1
    assert count == 95
    # There are 95 in total, and only 1 is known to be corrupted
    # but this image still exists
    assert len(images) == count
