import ase.build
import mincepy

import pyos
from pyos import psh

from dragonfruit import ionic_liquids

# pylint: disable=unused-argument


def test_adf_optimisation(pyos_project, historian: mincepy.Historian):
    """Test creating a task to optimise two molecules put together

    The folder structure we expect after creating the task is:
    ./mol1
    ./mol2
    ./mol1-mol2/mol1-mol2-[rand str] - joined molecule
    ./mol1-mol2/.runs/mol1-mol2-[rand str]/[task obj id]

    """
    # Create a molecule
    mol1 = ase.build.molecule("AlF3")

    # And give it a charge
    psh.save(mol1, "mol1")
    psh.meta - psh.s(mol1, presumed_charge=5)  # pylint: disable=expression-not-assigned

    opt = ionic_liquids.AdfOptimisation(mol1, {}, label="testmol")

    # Check that the charge is propsaged to the adfprep settings
    assert opt.atoms is mol1
    # The initial and final atoms should be a copy of mol1
    assert opt.final_atoms is not mol1
    assert opt.final_atoms == mol1
    assert opt.initial_atoms is not mol1
    assert opt.initial_atoms == mol1
    assert opt.initial_atoms is opt.final_atoms

    assert opt.label == "testmol"  # pylint: disable=comparison-with-callable

    # Check that all of the above is true after saving/loading
    opt_id = psh.save(opt)
    del opt
    opt = psh.load(opt_id)
    # Check that the charge is propsaged to the adfprep settings
    assert opt.atoms is mol1
    # The initial and final atoms should be a copy of mol1
    assert opt.final_atoms is not mol1
    assert opt.final_atoms == mol1
    assert opt.initial_atoms is not mol1
    assert opt.initial_atoms == mol1
    assert opt.initial_atoms is opt.final_atoms

    assert opt.label == "testmol"

    # Make sure we can overwrite the charge
    opt = ionic_liquids.AdfOptimisation(mol1, {"z": 2}, label="testmol", take_charge_from=None)
    assert (
        opt.settings.get(ionic_liquids.constants.ADFPREP_CHARGE) == 2
    )  # pylint: disable=no-member


def test_optimise_task(pyos_project, historian: mincepy.Historian):
    """Check basics of the PLAMS optimise task"""
    # Create a molecule
    mol1 = ase.build.molecule("AlF3")
    mol1_id = psh.save(mol1)

    task = ionic_liquids.adf_task_scripts.optimise_task(mol1)
    assert task.pyos_path == pyos.Path(f"./.runs/{mol1_id}/").resolve()
    plams_task = task.cmd.args[0]  # pylint: disable=no-member
    assert isinstance(plams_task, ionic_liquids.plams_scripts.PlamsTask)

    assert plams_task.atoms is mol1
    # The initial and final are copies
    assert plams_task.initial_atoms == mol1
    assert plams_task.final_atoms == mol1
