import ase.build
import mincepy
import minkipy

from pyos import db
from pyos import psh
from pyos import pathlib

from dragonfruit.ionic_liquids import adf_task_scripts

# pylint: disable=unused-argument


def test_optimise_pair_task(pyos_project, historian: mincepy.Historian):
    """Test creating a task to optimise two molecules put together

    The folder structure we expect after creating the task is:
    ./mol1
    ./mol2
    ./mol1-mol2/mol1-mol2-[rand str] - joined molecule
    ./mol1-mol2/.runs/mol1-mol2-[rand str]/[task obj id]

    """
    # Let's create a couple of molecules
    mol1 = ase.build.molecule("AlF3")
    mol2 = ase.build.molecule("CH3CH2NH2")

    psh.save(mol1, "mol1")
    psh.meta - psh.s(mol1, presumed_charge=5)  # pylint: disable=expression-not-assigned

    psh.save(mol2, "mol2")
    psh.meta - psh.s(mol2, presumed_charge=-2)  # pylint: disable=expression-not-assigned

    task = adf_task_scripts.optimise_pair_task(mol1, mol2)  # type: minkipy.Task

    # Now, let's do some checks
    assert isinstance(task, minkipy.Task)
    adf_task = task.cmd.args[0]  # pylint: disable=no-member
    joined = adf_task.atoms

    # Check that it set the charge correctly
    assert adf_task_scripts._get_presumed_charge(joined) == 3  # pylint: disable=protected-access

    # Check the folder layout
    joined_path = pathlib.Path(db.get_path(joined))
    pair_name = adf_task_scripts.get_pair_name(mol1, mol2)

    assert joined_path.name.startswith(pair_name)
    joined_name = joined_path.name

    base_dir = psh.ls("./")
    assert len(base_dir) == 3
    assert pathlib.Path("mol1") in base_dir
    assert base_dir["mol1"].obj is mol1

    assert pathlib.Path("mol2") in base_dir
    assert base_dir["mol2"].obj is mol2

    pair_path = pathlib.Path(pair_name).to_dir()
    assert pair_path in base_dir
    molecule_dir = base_dir[pair_path.name]

    molecule_path = pathlib.Path(joined_name)
    assert molecule_path in molecule_dir
    assert molecule_dir[joined_name].obj is joined

    runs_dir = molecule_dir[str(adf_task_scripts.DEFAULT_RUN_FOLDER)]
    task_dir = runs_dir[str(molecule_path.to_dir())]
    assert task.obj_id in task_dir


def test_optimise_pair_task_settings(
    pyos_project, historian: mincepy.Historian
):  # pylint: disable=invalid-name
    # Let's create a couple of molecules
    mol1 = ase.build.molecule("AlF3")
    mol2 = ase.build.molecule("CH3CH2NH2")

    task = adf_task_scripts.optimise_pair_task(mol1, mol2)  # type: minkipy.Task

    # Check that the default charge is correct
    adf_task = task.cmd.args[0]  # pylint: disable=no-member
    assert "z" not in adf_task.settings
