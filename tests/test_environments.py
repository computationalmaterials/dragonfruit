import pytest
from dragonfruit import environments


@pytest.fixture
def slurmjob(test_files_path):
    with open(test_files_path / "slurm" / "slurm_example.txt", "r") as file:
        return file.read()


@pytest.fixture
def job_settings(slurmjob):
    return environments.parse_slurm_job(slurmjob)


def test_parse_slurmjob(job_settings):
    assert isinstance(job_settings, dict)
    exp_keys = ["JobId", "JobName", "StdOut", "StdIn", "MailUser", "Partition"]
    assert all(key in job_settings for key in exp_keys)


def test_key_dots(job_settings):
    # MongoDB chokes if the key contains a period
    for key in job_settings:
        assert "." not in key


def test_parse_env(job_settings):
    env = environments.parse_slurm_environment(job_settings)
    assert env.scheduler == environments.SLURM
    assert "slurm-4655497.out" in env.stdout
    assert "slurm-4655497.out" in env.stderr
