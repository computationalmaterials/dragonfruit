import ase
import ase.db
import pytest
from pyos import psh
from dragonfruit import clease


@pytest.fixture
def bulk_init(pyos_project):
    clease.init_bulk(crystalstructure="sc", a=3.0)


@pytest.fixture
def init_fast_random(bulk_init, pyos_project, fast_clease_settings):
    """Fixture which initializes a bulk project, and creates some random structures"""

    def _init_fast_random(num_to_gen):
        return clease.new_random_structures(num_to_generate=num_to_gen, **fast_clease_settings)

    return _init_fast_random


def test_clease_project_init(pyos_project, bulk_init):
    settings = clease.get_structure_settings()

    assert settings["crystalstructure"] == "sc"
    assert settings["a"] == 3
    assert settings["type"] == "CEBulk"

    assert clease.root()


def test_clease_endpoints(concentration, bulk_init):
    endpoints = clease.new_endpoints(concentration)

    assert len(endpoints) == 2

    assert isinstance(endpoints[0], ase.Atoms)
    assert isinstance(endpoints[1], ase.Atoms)
    assert psh.oid(endpoints[0]) is not None, "atoms object is not saved!"

    initial = clease.get_atoms()
    assert len(initial) == 2
    assert psh.oid(initial[0]) is not None

    # Ask to generate the same ones again, these should be rejected as they already exist
    new_endpoints = clease.new_endpoints(concentration)
    assert not new_endpoints


def test_clease_random_structure(init_fast_random, fast_clease_settings):
    structures = init_fast_random(2)

    assert len(structures) == 2
    assert len(clease.get_atoms()) == 2

    new_structures = clease.new_random_structures(1, **fast_clease_settings)
    assert len(new_structures) == 1
    assert len(clease.get_atoms()) == 3


def test_clease_insert(init_fast_random):
    num_to_gen = 10
    init_fast_random(num_to_gen)

    # Just set the initial as final
    initials = clease.get_atoms()
    assert len(initials) == num_to_gen

    group = "vol-converged"
    finals = []
    for initial in initials:
        finals.append(initial.copy())
        clease.set_final(finals[-1], initial, group)

    results = clease.get_atoms(group)
    assert len(results) == num_to_gen
    for atoms in finals:
        assert atoms in results

    # Test the get_atoms()
    results = clease.get_atoms("not-there")
    assert len(results) == 0

    results = clease.get_atoms(group, initial=initials[:3])
    assert len(results) == 3

    # Add a final from one which already exists, and assert we no longer find old one
    finals.append(initials[0].copy())
    clease.set_final(finals[-1], initials[0], group)
    results = clease.get_atoms(group)
    assert len(results) == num_to_gen + 1
    results = clease.get_atoms(group, final_only=True)
    assert len(results) == num_to_gen


def test_clease_get_settings(tmp_path, concentration, init_fast_random):
    num_to_gen = 2
    structures = init_fast_random(num_to_gen)
    assert len(clease.get_atoms()) == num_to_gen

    # Just set the initial as final
    for structure in structures:
        clease.set_final(structure.copy(), structure, "vol-converged")

    # Now see if we can get the ce settings with the finals
    settings = clease.create_settings(
        init_args=dict(concentration=concentration, db_name=str(tmp_path / "clease.db")),
        final_group="vol-converged",
    )

    db = ase.db.connect(settings.db_name)
    assert len(tuple(db.select(gen=0))) == num_to_gen  # Initial
    assert len(tuple(db.select(struct_type="final"))) == num_to_gen  # Final


def test_set_initial(init_fast_random):
    structures = init_fast_random(3)

    for atoms in structures:
        clease.set_initial(atoms, directory="initial")

        meta = psh.meta(atoms)

        assert "sg" in meta
        assert "formula" in meta
        assert "volume" in meta

        assert meta["volume"] == pytest.approx(atoms.get_volume())

        assert "initial/" in meta["_directory"]
        assert meta["clease_group"] == "initial"


def test_set_final(init_fast_random):
    structures = init_fast_random(3)

    for atoms in structures:
        clease.set_final(atoms, atoms.copy(), directory="final_dir", group="final")

        meta = psh.meta(atoms)

        assert "sg" in meta
        assert "formula" in meta
        assert "volume" in meta

        assert meta["volume"] == pytest.approx(atoms.get_volume())

        assert "final_dir/" in meta["_directory"]
        assert meta["clease_group"] == "final"
