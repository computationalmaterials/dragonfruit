import clease
import pytest

# pylint: disable=redefined-outer-name


@pytest.fixture
def concentration():
    return clease.settings.Concentration(basis_elements=(("Cu", "Au"),))


@pytest.fixture
def fast_clease_settings(concentration: clease.settings.Concentration):
    """Generate fast clease settings just for testing"""
    return dict(
        size=(2, 2, 2),
        concentration=concentration,
        max_cluster_dia=[5],
    )
