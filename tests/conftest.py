import os
import pathlib

import ase
import mincepy
import pymongo
import pytest

DEFAULT_ARCHIVE_URI = "mongodb://127.0.0.1/dragonfruit_tests"
DEFAULT_RMQ_URI = "amqp://guest:guest@127.0.0.1/"

ENV_ARCHIVE_URI = "DRAGONFRUIT_TEST_ARCHIVE"
ENV_RMQ_URI = "DRAGONFRUIT_TEST_RMQ"

# pylint: disable=redefined-outer-name, import-outside-toplevel, invalid-name


@pytest.fixture
def mongo_archive():
    archive_uri = os.environ.get(ENV_ARCHIVE_URI, DEFAULT_ARCHIVE_URI)
    client = pymongo.MongoClient(archive_uri)
    db = client.get_default_database()
    mongo_archive = mincepy.mongo.MongoArchive(db)
    yield mongo_archive
    client.drop_database(db)


@pytest.fixture(autouse=True)
def historian(mongo_archive: mincepy.mongo.MongoArchive):
    historian = mincepy.Historian(mongo_archive)
    mincepy.set_historian(historian)
    try:
        yield historian
    finally:
        mincepy.set_historian(None)


@pytest.fixture
def test_files_path():
    return pathlib.Path(__file__).parents[0] / "files"


@pytest.fixture
def atoms():
    depth = 2.9
    length = 10.0
    wire = ase.Atoms(
        "Au",
        positions=[[0, length / 2, length / 2]],
        cell=[depth, length, length],
        pbc=[1, 0, 0],
    )

    return wire


@pytest.fixture
def pyos_project(historian: mincepy.Historian):  # pylint: disable=unused-argument
    """Fixture that enables pyOS functionality to be tested"""
    import pyos

    pyos.init()
    clease_test = pyos.Path("clease_test/")
    with pyos.pathlib.working_path(clease_test):
        yield clease_test
    pyos.reset()
