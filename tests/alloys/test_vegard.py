import pytest
import numpy as np
import ase
from ase.build import bulk
from dragonfruit.alloys import vegard


@pytest.fixture
def size():
    return (2, 2, 2)


@pytest.fixture
def a():
    # Default lattice parameter
    return 4.0


@pytest.fixture
def nacl(size, a):
    """Simple cubic NaCl fcc cell with a=1."""
    # Note, this is not quite the same as using rocksalt
    atoms = bulk("Na", crystalstructure="fcc", a=a, cubic=True) * size
    atoms.symbols[1::2] = "Cl"  # Make every other atom Cl
    eye = np.eye(3) * a * size  # Expected cell
    assert np.allclose(eye, atoms.get_cell())
    return atoms


@pytest.fixture
def elementals(a):
    """Some dumy elementals for testing"""
    elems = [
        dict(name="Na", a=a - 0.2),
        dict(name="Cl", a=a + 0.75),
        dict(name="Zn", a=a + 0.5),
    ]
    crystalstructure = "fcc"
    default = dict(crystalstructure=crystalstructure, cubic=True)

    elems_atoms = []
    for elem_d in elems:
        name = elem_d.pop("name")
        settings = default.copy()
        settings.update(**elem_d)
        atoms = bulk(name, **settings)
        elems_atoms.append(atoms)
    return elems_atoms


def test_get_concentrations(nacl):
    # Test the fixture
    conc = vegard.get_concentrations(nacl)
    assert conc == pytest.approx({"Na": 0.5, "Cl": 0.5})

    # Set all symbosl to 'Na'
    nacl.symbols = "Na"
    conc = vegard.get_concentrations(nacl)
    assert conc == pytest.approx({"Na": 1})

    # Test a custom atoms object with a different ratio
    atoms = ase.Atoms(symbols=["Na", "Na", "Cl"])
    conc = vegard.get_concentrations(atoms)
    assert conc == pytest.approx({"Na": 2 / 3, "Cl": 1 / 3})


def test_get_elemental_cells(nacl, elementals):
    assert len(elementals) == 3
    elementals_dct = vegard.get_elemental_cells(nacl, elementals)
    assert len(elementals_dct) == 2
    assert "Zn" not in elementals_dct
    assert "Na" in elementals_dct
    assert "Cl" in elementals_dct

    assert np.allclose(elementals_dct["Na"], elementals[0].get_cell())
    assert np.allclose(elementals_dct["Cl"], elementals[1].get_cell())


def test_make_vegard_atoms(nacl, elementals, size):
    vegard_atoms = vegard.make_vegard_atoms(nacl, elementals, size=size)
    # Test we also moved the atoms, and not just the cell
    assert not np.allclose(nacl.get_positions(), vegard_atoms.get_positions())

    a_lp = np.linalg.norm(vegard_atoms.get_cell(), axis=1)[0]
    # Calculate expected
    expected_a_na = np.linalg.norm(elementals[0].get_cell(), axis=1)[0]
    expected_a_cl = np.linalg.norm(elementals[1].get_cell(), axis=1)[0]
    expected_lp = (expected_a_na + expected_a_cl) / 2
    expected_lp *= size[0]

    assert a_lp == pytest.approx(expected_lp)
    expected_eye = np.eye(3) * expected_lp  # Expected identity matrix
    assert np.allclose(vegard_atoms.get_cell(), expected_eye)
