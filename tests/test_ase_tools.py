import pytest
import ase

from dragonfruit import ase_utils as utils
import dragonfruit.vasp as mv


def test_get_visualizable(atoms):
    assert isinstance(utils.get_visualizable(atoms), ase.Atoms)
    atoms_list = [atoms]
    assert utils.get_visualizable(atoms_list) is atoms_list

    # Test some cases which should fail
    for obj in [1, "hello", [], None]:
        with pytest.raises(TypeError):
            utils.get_visualizable(obj)

    # Test some instances of AseVisualizable
    for obj in [mv.VaspRun(atoms, {}), mv.VaspTask(atoms, {})]:
        utils.get_visualizable(obj)
