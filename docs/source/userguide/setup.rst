Dragonfruit Setup
==================

The traditional Dragonfruit workflow works in a git-like repository
in the `pyOS`_ framework, within a database as configured by
`minkipy`_.

.. todo:: How to configure the minkipy settings file.

.. _pyOS: https://pyos.readthedocs.io/en/latest/
.. _minkipy: https://github.com/muhrin/minkipy
.. _mincepy: https://mincepy.readthedocs.io/

Setting up a Dragonfruit Repository
------------------------------------

Let's try setting up a the first repository,
from where we will be doing calculations in our database.
Once you have properly set up your ``minkipy``, connecting is
quite simple. First we launch an IPython console:

.. code-block:: sh

    $ ipython


test

.. code-block:: python

    import minkipy
    minkipy.workon()
