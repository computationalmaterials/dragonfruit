Getting Started
===============

We here present some examples of how to use the Dragonfruit workflow.
In the first tutorial, we will guide you through using the workflow with the
`VASP`_ software.

Note, that this requires already having a working VASP setup. Furthermore,
VASP is executed using the `ASE`_ interface, and we therefore refer to the
`ASE VASP wiki`_ for more information on how to configure your VASP setup to
be used by the ASE VASP calculator.

Furthermore, it is assumed you have already prepared your MongoDB and
RabbitMQ services. Instructions for installing MongoDB can be found in the
`mongo manual <https://docs.mongodb.com/manual/administration/install-community/>`_,
and RabbitMQ at `the rabbit website <https://www.rabbitmq.com/download.html>`_.


.. _VASP: https://www.vasp.at/
.. _ASE: https://wiki.fysik.dtu.dk/ase/
.. _ASE VASP wiki: https://wiki.fysik.dtu.dk/ase/ase/calculators/vasp.html

.. toctree::
   :maxdepth: 2

   quickstart.ipynb
