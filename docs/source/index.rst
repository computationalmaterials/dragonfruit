.. dragonfruit documentation master file, created by
   sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Dragonfruit's documentation!
========================================

Dragonfruit is a workflow built on top of `pyOS`_ and `mincepy`_ for materials science.
It allows for multiple users to work together on a single central database, and
collaboratively run tasks on e.g. a HPC cluster, to allow for more efficient resource
usage and distribution.

Under the hood, the it uses `MongoDB`_ for the centralized database and `RabbitMQ`_
for the job messaging system.

.. _pyOS: https://pyos.readthedocs.io/en/latest/
.. _mincepy: https://mincepy.readthedocs.io/
.. _MongoDB: https://www.mongodb.com/
.. _RabbitMQ: https://www.rabbitmq.com/


.. toctree::
   :maxdepth: 1

   ./userguide/userguide_index
   ./api/api
